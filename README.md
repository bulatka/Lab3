 # Asm. Транслятор и модель

- Хафизов Булат Ленарович P33131.
- `asm | cisc | harv | mc | tick | struct | stream | mem | prob2`

## Язык программирования

``` ebnf
program ::= line "\n" program

line ::= label
       | command
       | section
       | variable

label ::= label_name ":\n"

command ::= "\t" operation_2_args " " operand " " operand |
            "\t" operation_1_arg " " operand |
            "\t" operation_0_args |

operation_2_args ::= "mov" | "cmp" | "test" | "add"

operation_1_arg ::= "in" | "out" | "out#" | "out_char" | "jmp" | "je"

operation_0_args ::= "hlt"

operand ::= variable_name | label_name | number

section ::= "." section_name

section_name ::= "text" | "data"

variable ::= var_name " " var_type " " var_value

var_type ::= "num" | "str"

label_name ::= [a-z_]+
          
var_name ::= [a-z_]+[0-9]+

var_value ::= number | string

string ::= "[a-zA-z0-9!@#$%^&*().,]+"

number ::= [-2^32; 2^32 - 1]

```

Код выполняется последовательно. Операции:

- `mov <arg1> <arg2>` -- присваивает второму аргументу значение первого аргумента
- `cmp <arg1> <arg2>` -- производится вычитание из второго аргумента первого с целью их сравнения
- `test <arg1> <arg2>` -- выполнить логическое И между всеми битами двух операндов
- `add <arg1> <arg2>` -- прибавить первый аргумент ко второму и перезаписать его в память
- `in <arg>` -- прочитать символ из потока ввода
- `out <arg>` -- вывести число в потока вывода
- `out# <arg>` -- вывести с косвенной адресацией
- `out_char <arg>` -- вывести символ в потока вывода
- `jmp <label>` -- безусловный переход по адресу метки 
- `je <label>` -- переход по адресу метки, в случае, если выставлен zero-флаг
- `hlt` -- завершить выполнение программы


## Организация памяти
Модель памяти процессора:

1. Память команд. Машинное слово -- не определено. Реализуется списком словарей, описывающих инструкции (одно слово -- одна ячейка).
2. Память данных. Машинное слово -- 32 бита, знаковое. Линейное адресное пространство. Реализуется списком чисел.

Строки, объявленные пользователем распеделяются по памяти один символ на ячейку.

```text
        Instruction memory
+-----------------------------+
| 00  : cmp last divisor      |
| 01  : je end                |
|    ...                      | 
| n   : hlt                   |
|    ...                      |
+-----------------------------+

        Data memory
+-----------------------------+
| 00  : number                |
| 01  : number                |
|    ...                      |
| 100 : number                |
|    ...                      |
+-----------------------------+
```

## Система команд

Особенности процессора:

- Машинное слово -- 32 бит, знаковое.
- Память данных:
    - адресуется через регистр `data_address`;
    - может быть записана:
        - из аккумулятора `acc`;
        - с буферного регистра `br`;
    - может быть прочитана в аккумулятор `acc`:
- Регистр аккумулятора `acc`:
    - может быть подан на вывод;
    - используется как флаг (сравнение с 0);
- Ввод-вывод -- memory-mapped через резервированные ячейки памяти, символьный.
- `program_counter` -- счётчик команд:
    - инкрементируется после каждой инструкции или перезаписывается инструкцией перехода.

### Набор инструкции

| Syntax         | Mnemonic        | Кол-во тактов | Comment                           |
|:---------------|:----------------|---------------|:----------------------------------|
| `mov`          | mov             | 4             | см. язык                          |
| `cmp`          | cmp             | 4             | см. язык                          |
| `test`         | test            | 4             | см. язык                          |
| `add`          | add             | 5             | см. язык                          |
| `in`           | in              | 3             | см. язык                          |
| `out`          | out             | 4             | см. язык                          |
| `out#`         | out_rel         | 5             | см. язык                          |
| `out_char`     | out_char        | 4             | см. язык                          |
| `jmp`          | jmp             | 2             | см. язык                          |
| `je`           | je              | 2             | см. язык                          |
| `hlt`          | hlt             | 2             | см. язык                          |

### Кодирование инструкций

- Машинный код сериализуется в список JSON.
- Один элемент списка, одна инструкция.
- Индекс списка -- адрес инструкции. Используется для команд перехода.

Пример:

```json
[
   {
        "opcode": "mov",
        "addr": 0,
        "args": [
            1,
            2
        ]
    }
]
```

где:

- `opcode` -- строка с кодом операции;
- `addr` -- адрес команды. Очередность выполнения команды, начиная с первой в секции `.text`. Нумерация с нуля;
- `args` -- адреса аргументов от первого до последнего. Если их нет, то список пустой.

Типы данные в модуле [isa](./isa.py), где:

- `Opcode` -- перечисление кодов операций.

### Микропрограммное управление

- Каждая инструкция разбита на последовательность микрокоманд.
- Микрокоманда представляет из себя набор сигналов, подаваемых на линии. 
- Память микрокоманд `mc_memory` находится в `ControlUnit`.
- Каждая микрокоманда выполняется за такт, соответственно моделирование происходит на уровне тиков.
- Исполнение каждой инструкции заканчивается микрокомандой перехода на другую инструкцию, либо завершением программы.

| Microcomand                      | Comment                                     |
|:---------------------------------|:--------------------------------------------|
| `latch_microcode`                | Выборка микрокоманды по команде             |
| `read_first_arg`                 | Чтение первого аргумента команды            |
| `write_from_input`               | Запись в память из входного буфера          |
| `read_from_acc_addr`             | Чтение из памяти по адресу в аккумуляторе   |
| `write_acc_by_second_arg`        | Запись в память по адресу второго аргумента |
| `alu_cmp`/`alu_test`/`alu_add`   | Выбор операции АЛУ                          |
| `out_num`                        | Сигнал вывода числа                         |
| `out_char`                       | Сигнал вывода символа                       |
| `next_instr`                     | Выбор следующей инструкции                  |
| `stop_program`                   | Завершение программы                        |

## Транслятор

Интерфейс командной строки: `translator.py <input_file> <target_file>`

Реализовано в модуле: [translator](./translator.py)

Этапы трансляции (функция `translate`):
1. Трансформирование текста в последовательность значимых машинных слов.
    - Переменные:
        - На этапе трансляции создается файл с данными `data_file`, в который записываются значения всех переменных. Команды оперируют адресами на них
        - Задаётся либо числовым значением, либо указателем на начало строки (используя string)
2. Проверка корректности программы (наличие нужных секций, корректность меток, переменных и т.д.).
3. Генерация машинного кода.

Правила генерации машинного кода:

- одно слово языка -- одна инструкция;
- для команд, однозначно соответствующих инструкциям -- прямое отображение;

## Модель процессора

Реализовано в модуле: [machine](./machine.py).


### DataPath

``` text
         latch_data_addr    wr                                             input
               |            |                                                |
               V            V                                                |
            +-----+    +--------+                                            V
address --->| ADR +--->|        |---------------------------+----------->+-------+<--------------------+
            +-----+    |        |                           |     sel -->|  MUX  |                     |
                       |        |                           |            +-------+                     |
                       |        |                           |                |                         |
                       |  Data  |                           |       +--------|-------------------------+
                       | Memory |                           |       V        V                         |
                       |        |                           |   +------+ +------+                      |
                       |        |              latch_br ------->|  BR  | |  AC  |<--- latch_acc        |
                       |        |      +-----+              |   +------+ +------+                      |
                       |        |      |     |<------------------+          |                          |
                       |        |<-----| MUX |              |    |   +------+--------------------------------> output
                       |        |      |     |<----------------------|      |                          |
                       +--------+      +-----+              |    |   |      +--------------------------------> is_zero
                                          ^                 |    |   |   0                             |
                                          |                 |    |   |   |                             |           
                                        sel_wr              V    V   V   V                             |
                                                         +-------+ +-------+                           |
                                            sel_left --->|  MUX  | |  MUX  |<--- sel_right             |
                                                         +-------+ +-------+                           |
                                                             |         |                               |
                                                             V         V                               |
                                                         +-----------------+                           |
                                                         |       ALU       |<--- alu_sel_op            |
                                                         +-----------------+                           |
                                                                  |                                    |
                                                                  +------------------------------------+
```

Реализован в классе `DataPath`.

Сигналы (обрабатываются за один такт, реализованы в виде методов класса):

- `wr` -- записать в data_memory
- `latch_data_address` -- защёлкнуть значение в `data_address`
- `latch_acc` -- защёлкнуть значение в `acc`
- `latch_br` -- защёлкнуть значение в `br`
- `sel` -- сигнал мультиплексору для выбора значения, подаваемого в аккумулятор
- `sel_wr` -- сигнал мультиплексору для выбора значения, записываемого в память
- `sel_left` -- сигнал мультиплексору для выбора значения, подаваемого на левый вход АЛУ
- `sel_right` -- сигнал мультиплексору для выбора значения, подаваемого на правый вход АЛУ
- `alu_sel_op` -- сигнал выбора операции в АЛУ

Флаги:

- `is_zero` -- лежит ли в аккумуляторе 0.


### ControlUnit

``` text
+-----------(+1)-------------------+
|                                  |
|      +-----+         +----+      |      +--------------+
+----->|     |         |    |      |      |              |
       | MUX |-------->| PC |------+----->| Program Data |
+----->|     |         |    |             |              |
|      +-----+         +----+             +--------------+
|         ^                                  |
|         |                                  |
+------------------(select-arg)--------------+
          |                                  V
       sel_next                           +-------------+  
          |                               | Instruction |
          |                               |   Decoder   |
          |                               +-------------+
          |                                      |
          |                                      |    +-----+
          |                                      V    V     |
          |                                +-----------+    |
          |                    +---------->|    MUX    |   (+1)
          |                    |           +-----------+    |
          |             fetch_next_inst          |          |
          |                    |                 V          |
          |                    |           +-----------+    |     +-----------+
          |                    |           |MC  Pointer|----+---->| MC Memory |
          |                    |           +-----------+          +-----------+
          |                  +-----+                                    |
          +------------------| MCR |<-----------------------------------+
                             +-----+
                                |
                             signals
                                V
                           +------+
              input ------>| Data |
                           | Path |------> output
                           +------+                           
```

Реализован в классе `ControlUnit`.

- Микрокод - непосредственно выводимые на сигнальные линии значения. Сигналы реализованы в виде функций на python
- Моделирование на уровне тактов.
- Трансляция инструкции в последовательность микрокоманд: `decode_and_execute_instruction`. На схеме - Instruction Decoder


Особенности работы модели:

- Для журнала состояний процессора используется стандартный модуль logging.
- Количество инструкций для моделирования ограничено hardcoded константой.
- Поддержка сложных многотактовых иснтрукий, осуществляющих и обращения к памяти и арифметику в рамках одной команды, как в CISC-архитектурах
- Небольшое число рабочих регистров (аккумулятор и буфферный регистр) 
- Остановка моделирования осуществляется при помощи исключений:
    - `EOFError` -- если нет данных для чтения из порта ввода-вывода;
    - `StopIteration` -- если выполнена инструкция `halt`.
- Управление симуляцией реализовано в функции `simulate`.


## Апробация

В качестве тестов для `machine` использовано 3 теста:

1. [hello world](examples/hello.asm) -- программа, выводящая "hello world".
2. [cat](examples/cat.asm) -- программа `cat`, повторяем ввод на выводе.
3. [prob2](examples/prob2.asm) -- программа, решающая пятую задачу Эйлера.


В качестве тестов для `translator` использовано 3 теста:
1. [hello world](examples/hello.asm) -- программа, выводящая "hello world".
2. [cat](examples/cat.asm) -- программа `cat`, повторяем ввод на выводе.
3. [prob2](examples/prob2.asm) -- программа, решающая пятую задачу Эйлера.


Интеграционные тесты реализованы тут: [machine_integration_test](machine_integration_test.py) и 
[translator_integration_test](translator_integration_test.py)

CI:

``` yaml
lab3-example:
  stage: test
  image:
    name: python-tools
    entrypoint: [""]
  script:
    - python3-coverage run -m pytest --verbose
    - find . -type f -name "*.py" | xargs -t python3-coverage report
    - find . -type f -name "*.py" | xargs -t pep8 --ignore=E501
    - find . -type f -name "*.py" | xargs -t pylint
```

где:

- `python3-coverage` -- формирование отчёта об уровне покрытия исходного кода.
- `pytest` -- утилита для запуска тестов.
- `pep8` -- утилита для проверки форматирования кода. `E501` (длина строк) отключено, но не следует этим злоупотреблять.
- `pylint` -- утилита для проверки качества кода. Некоторые правила отключены в отдельных модулях с целью упрощения кода.
- Docker image `python-tools` включает в себя все перечисленные утилиты. Его конфигурация: [Dockerfile](./Dockerfile).

Пример использования и журнал работы процессора на примере `cat`:

``` console
> cat examples/cat.asm
.data:
    num tmp 0
.text:
loop: 
    in tmp
    out_char tmp
	jmp loop
end: 
    hlt
> cat examples/input.txt
Good news, everyone!
> ./translator.py examples/cat.asm cat_code.out
source LoC: 14 code instr: 4
> cat target.out
[
    {
        "opcode": "in",
        "addr": 0,
        "args": [
            0
        ]
    },
    {
        "opcode": "out_char",
        "addr": 1,
        "args": [
            0
        ]
    },
    {
        "opcode": "jmp",
        "addr": 2,
        "args": [
            0
        ]
    },
    {
        "opcode": "hlt",
        "addr": 3,
        "args": []
    }
]
> ./machine.py examples/cat_code.out examples/input.txt
DEBUG:root:{TICK: 0, ADDR: 0, PC: 0, OUT: 72, ACC: 0} 
DEBUG:root:{TICK: 1, ADDR: 13, PC: 0, OUT: 0, ACC: 0} 
DEBUG:root:{TICK: 2, ADDR: 0, PC: 0, OUT: 72, ACC: 72} 
DEBUG:root:output: '' << 'H'
DEBUG:root:{TICK: 3, ADDR: 0, PC: 0, OUT: 72, ACC: 72} 
DEBUG:root:{TICK: 4, ADDR: 0, PC: 1, OUT: 72, ACC: 72} 
DEBUG:root:{TICK: 5, ADDR: 14, PC: 1, OUT: 1, ACC: 1} 
DEBUG:root:{TICK: 6, ADDR: 13, PC: 1, OUT: 0, ACC: 1} 
DEBUG:root:{TICK: 7, ADDR: 13, PC: 1, OUT: 1, ACC: 1} 
DEBUG:root:{TICK: 8, ADDR: 13, PC: 2, OUT: 1, ACC: 1} 
DEBUG:root:{TICK: 9, ADDR: 13, PC: 2, OUT: 1, ACC: 1} 
DEBUG:root:{TICK: 10, ADDR: 15, PC: 2, OUT: 12, ACC: 11} 
DEBUG:root:{TICK: 11, ADDR: 15, PC: 3, OUT: 12, ACC: 11} 
DEBUG:root:{TICK: 12, ADDR: 15, PC: 4, OUT: 12, ACC: 11} 
DEBUG:root:{TICK: 13, ADDR: 15, PC: 0, OUT: 12, ACC: 11} 
DEBUG:root:{TICK: 14, ADDR: 13, PC: 0, OUT: 1, ACC: 1} 
DEBUG:root:{TICK: 15, ADDR: 1, PC: 0, OUT: 101, ACC: 101} 
DEBUG:root:output: 'H' << 'e'
DEBUG:root:{TICK: 16, ADDR: 1, PC: 0, OUT: 101, ACC: 101} 
DEBUG:root:{TICK: 17, ADDR: 1, PC: 1, OUT: 101, ACC: 101} 
DEBUG:root:{TICK: 18, ADDR: 14, PC: 1, OUT: 1, ACC: 1} 
DEBUG:root:{TICK: 19, ADDR: 13, PC: 1, OUT: 1, ACC: 2} 
DEBUG:root:{TICK: 20, ADDR: 13, PC: 1, OUT: 2, ACC: 2} 
DEBUG:root:{TICK: 21, ADDR: 13, PC: 2, OUT: 2, ACC: 2} 
DEBUG:root:{TICK: 22, ADDR: 13, PC: 2, OUT: 2, ACC: 2} 
DEBUG:root:{TICK: 23, ADDR: 15, PC: 2, OUT: 12, ACC: 10} 
DEBUG:root:{TICK: 24, ADDR: 15, PC: 3, OUT: 12, ACC: 10} 
DEBUG:root:{TICK: 25, ADDR: 15, PC: 4, OUT: 12, ACC: 10} 
DEBUG:root:{TICK: 26, ADDR: 15, PC: 0, OUT: 12, ACC: 10} 
DEBUG:root:{TICK: 27, ADDR: 13, PC: 0, OUT: 2, ACC: 2} 
DEBUG:root:{TICK: 28, ADDR: 2, PC: 0, OUT: 108, ACC: 108} 
DEBUG:root:output: 'He' << 'l'
DEBUG:root:{TICK: 29, ADDR: 2, PC: 0, OUT: 108, ACC: 108} 
DEBUG:root:{TICK: 30, ADDR: 2, PC: 1, OUT: 108, ACC: 108} 
DEBUG:root:{TICK: 31, ADDR: 14, PC: 1, OUT: 1, ACC: 1} 
DEBUG:root:{TICK: 32, ADDR: 13, PC: 1, OUT: 2, ACC: 3} 
DEBUG:root:{TICK: 33, ADDR: 13, PC: 1, OUT: 3, ACC: 3} 
DEBUG:root:{TICK: 34, ADDR: 13, PC: 2, OUT: 3, ACC: 3} 
DEBUG:root:{TICK: 35, ADDR: 13, PC: 2, OUT: 3, ACC: 3} 
DEBUG:root:{TICK: 36, ADDR: 15, PC: 2, OUT: 12, ACC: 9} 
DEBUG:root:{TICK: 37, ADDR: 15, PC: 3, OUT: 12, ACC: 9} 
DEBUG:root:{TICK: 38, ADDR: 15, PC: 4, OUT: 12, ACC: 9} 
DEBUG:root:{TICK: 39, ADDR: 15, PC: 0, OUT: 12, ACC: 9} 
DEBUG:root:{TICK: 40, ADDR: 13, PC: 0, OUT: 3, ACC: 3} 
DEBUG:root:{TICK: 41, ADDR: 3, PC: 0, OUT: 108, ACC: 108} 
DEBUG:root:output: 'Hel' << 'l'
DEBUG:root:{TICK: 42, ADDR: 3, PC: 0, OUT: 108, ACC: 108} 
DEBUG:root:{TICK: 43, ADDR: 3, PC: 1, OUT: 108, ACC: 108} 
DEBUG:root:{TICK: 44, ADDR: 14, PC: 1, OUT: 1, ACC: 1} 
DEBUG:root:{TICK: 45, ADDR: 13, PC: 1, OUT: 3, ACC: 4} 
DEBUG:root:{TICK: 46, ADDR: 13, PC: 1, OUT: 4, ACC: 4} 
DEBUG:root:{TICK: 47, ADDR: 13, PC: 2, OUT: 4, ACC: 4} 
DEBUG:root:{TICK: 48, ADDR: 13, PC: 2, OUT: 4, ACC: 4} 
DEBUG:root:{TICK: 49, ADDR: 15, PC: 2, OUT: 12, ACC: 8} 
DEBUG:root:{TICK: 50, ADDR: 15, PC: 3, OUT: 12, ACC: 8} 
DEBUG:root:{TICK: 51, ADDR: 15, PC: 4, OUT: 12, ACC: 8} 
DEBUG:root:{TICK: 52, ADDR: 15, PC: 0, OUT: 12, ACC: 8} 
DEBUG:root:{TICK: 53, ADDR: 13, PC: 0, OUT: 4, ACC: 4} 
DEBUG:root:{TICK: 54, ADDR: 4, PC: 0, OUT: 111, ACC: 111} 
DEBUG:root:output: 'Hell' << 'o'
DEBUG:root:{TICK: 55, ADDR: 4, PC: 0, OUT: 111, ACC: 111} 
DEBUG:root:{TICK: 56, ADDR: 4, PC: 1, OUT: 111, ACC: 111} 
DEBUG:root:{TICK: 57, ADDR: 14, PC: 1, OUT: 1, ACC: 1} 
DEBUG:root:{TICK: 58, ADDR: 13, PC: 1, OUT: 4, ACC: 5} 
DEBUG:root:{TICK: 59, ADDR: 13, PC: 1, OUT: 5, ACC: 5} 
DEBUG:root:{TICK: 60, ADDR: 13, PC: 2, OUT: 5, ACC: 5} 
DEBUG:root:{TICK: 61, ADDR: 13, PC: 2, OUT: 5, ACC: 5} 
DEBUG:root:{TICK: 62, ADDR: 15, PC: 2, OUT: 12, ACC: 7} 
DEBUG:root:{TICK: 63, ADDR: 15, PC: 3, OUT: 12, ACC: 7} 
DEBUG:root:{TICK: 64, ADDR: 15, PC: 4, OUT: 12, ACC: 7} 
DEBUG:root:{TICK: 65, ADDR: 15, PC: 0, OUT: 12, ACC: 7} 
DEBUG:root:{TICK: 66, ADDR: 13, PC: 0, OUT: 5, ACC: 5} 
DEBUG:root:{TICK: 67, ADDR: 5, PC: 0, OUT: 32, ACC: 32} 
DEBUG:root:output: 'Hello' << ' '
DEBUG:root:{TICK: 68, ADDR: 5, PC: 0, OUT: 32, ACC: 32} 
DEBUG:root:{TICK: 69, ADDR: 5, PC: 1, OUT: 32, ACC: 32} 
DEBUG:root:{TICK: 70, ADDR: 14, PC: 1, OUT: 1, ACC: 1} 
DEBUG:root:{TICK: 71, ADDR: 13, PC: 1, OUT: 5, ACC: 6} 
DEBUG:root:{TICK: 72, ADDR: 13, PC: 1, OUT: 6, ACC: 6} 
DEBUG:root:{TICK: 73, ADDR: 13, PC: 2, OUT: 6, ACC: 6} 
DEBUG:root:{TICK: 74, ADDR: 13, PC: 2, OUT: 6, ACC: 6} 
DEBUG:root:{TICK: 75, ADDR: 15, PC: 2, OUT: 12, ACC: 6} 
DEBUG:root:{TICK: 76, ADDR: 15, PC: 3, OUT: 12, ACC: 6} 
DEBUG:root:{TICK: 77, ADDR: 15, PC: 4, OUT: 12, ACC: 6} 
DEBUG:root:{TICK: 78, ADDR: 15, PC: 0, OUT: 12, ACC: 6} 
DEBUG:root:{TICK: 79, ADDR: 13, PC: 0, OUT: 6, ACC: 6} 
DEBUG:root:{TICK: 80, ADDR: 6, PC: 0, OUT: 119, ACC: 119} 
DEBUG:root:output: 'Hello ' << 'w'
DEBUG:root:{TICK: 81, ADDR: 6, PC: 0, OUT: 119, ACC: 119} 
DEBUG:root:{TICK: 82, ADDR: 6, PC: 1, OUT: 119, ACC: 119} 
DEBUG:root:{TICK: 83, ADDR: 14, PC: 1, OUT: 1, ACC: 1} 
DEBUG:root:{TICK: 84, ADDR: 13, PC: 1, OUT: 6, ACC: 7} 
DEBUG:root:{TICK: 85, ADDR: 13, PC: 1, OUT: 7, ACC: 7} 
DEBUG:root:{TICK: 86, ADDR: 13, PC: 2, OUT: 7, ACC: 7} 
DEBUG:root:{TICK: 87, ADDR: 13, PC: 2, OUT: 7, ACC: 7} 
DEBUG:root:{TICK: 88, ADDR: 15, PC: 2, OUT: 12, ACC: 5} 
DEBUG:root:{TICK: 89, ADDR: 15, PC: 3, OUT: 12, ACC: 5} 
DEBUG:root:{TICK: 90, ADDR: 15, PC: 4, OUT: 12, ACC: 5} 
DEBUG:root:{TICK: 91, ADDR: 15, PC: 0, OUT: 12, ACC: 5} 
DEBUG:root:{TICK: 92, ADDR: 13, PC: 0, OUT: 7, ACC: 7} 
DEBUG:root:{TICK: 93, ADDR: 7, PC: 0, OUT: 111, ACC: 111} 
DEBUG:root:output: 'Hello w' << 'o'
DEBUG:root:{TICK: 94, ADDR: 7, PC: 0, OUT: 111, ACC: 111} 
DEBUG:root:{TICK: 95, ADDR: 7, PC: 1, OUT: 111, ACC: 111} 
DEBUG:root:{TICK: 96, ADDR: 14, PC: 1, OUT: 1, ACC: 1} 
DEBUG:root:{TICK: 97, ADDR: 13, PC: 1, OUT: 7, ACC: 8} 
DEBUG:root:{TICK: 98, ADDR: 13, PC: 1, OUT: 8, ACC: 8} 
DEBUG:root:{TICK: 99, ADDR: 13, PC: 2, OUT: 8, ACC: 8} 
DEBUG:root:{TICK: 100, ADDR: 13, PC: 2, OUT: 8, ACC: 8} 
DEBUG:root:{TICK: 101, ADDR: 15, PC: 2, OUT: 12, ACC: 4} 
DEBUG:root:{TICK: 102, ADDR: 15, PC: 3, OUT: 12, ACC: 4} 
DEBUG:root:{TICK: 103, ADDR: 15, PC: 4, OUT: 12, ACC: 4} 
DEBUG:root:{TICK: 104, ADDR: 15, PC: 0, OUT: 12, ACC: 4} 
DEBUG:root:{TICK: 105, ADDR: 13, PC: 0, OUT: 8, ACC: 8} 
DEBUG:root:{TICK: 106, ADDR: 8, PC: 0, OUT: 114, ACC: 114} 
DEBUG:root:output: 'Hello wo' << 'r'
DEBUG:root:{TICK: 107, ADDR: 8, PC: 0, OUT: 114, ACC: 114} 
DEBUG:root:{TICK: 108, ADDR: 8, PC: 1, OUT: 114, ACC: 114} 
DEBUG:root:{TICK: 109, ADDR: 14, PC: 1, OUT: 1, ACC: 1} 
DEBUG:root:{TICK: 110, ADDR: 13, PC: 1, OUT: 8, ACC: 9} 
DEBUG:root:{TICK: 111, ADDR: 13, PC: 1, OUT: 9, ACC: 9} 
DEBUG:root:{TICK: 112, ADDR: 13, PC: 2, OUT: 9, ACC: 9} 
DEBUG:root:{TICK: 113, ADDR: 13, PC: 2, OUT: 9, ACC: 9} 
DEBUG:root:{TICK: 114, ADDR: 15, PC: 2, OUT: 12, ACC: 3} 
DEBUG:root:{TICK: 115, ADDR: 15, PC: 3, OUT: 12, ACC: 3} 
DEBUG:root:{TICK: 116, ADDR: 15, PC: 4, OUT: 12, ACC: 3} 
DEBUG:root:{TICK: 117, ADDR: 15, PC: 0, OUT: 12, ACC: 3} 
DEBUG:root:{TICK: 118, ADDR: 13, PC: 0, OUT: 9, ACC: 9} 
DEBUG:root:{TICK: 119, ADDR: 9, PC: 0, OUT: 108, ACC: 108} 
DEBUG:root:output: 'Hello wor' << 'l'
DEBUG:root:{TICK: 120, ADDR: 9, PC: 0, OUT: 108, ACC: 108} 
DEBUG:root:{TICK: 121, ADDR: 9, PC: 1, OUT: 108, ACC: 108} 
DEBUG:root:{TICK: 122, ADDR: 14, PC: 1, OUT: 1, ACC: 1} 
DEBUG:root:{TICK: 123, ADDR: 13, PC: 1, OUT: 9, ACC: 10} 
DEBUG:root:{TICK: 124, ADDR: 13, PC: 1, OUT: 10, ACC: 10} 
DEBUG:root:{TICK: 125, ADDR: 13, PC: 2, OUT: 10, ACC: 10} 
DEBUG:root:{TICK: 126, ADDR: 13, PC: 2, OUT: 10, ACC: 10} 
DEBUG:root:{TICK: 127, ADDR: 15, PC: 2, OUT: 12, ACC: 2} 
DEBUG:root:{TICK: 128, ADDR: 15, PC: 3, OUT: 12, ACC: 2} 
DEBUG:root:{TICK: 129, ADDR: 15, PC: 4, OUT: 12, ACC: 2} 
DEBUG:root:{TICK: 130, ADDR: 15, PC: 0, OUT: 12, ACC: 2} 
DEBUG:root:{TICK: 131, ADDR: 13, PC: 0, OUT: 10, ACC: 10} 
DEBUG:root:{TICK: 132, ADDR: 10, PC: 0, OUT: 100, ACC: 100} 
DEBUG:root:output: 'Hello worl' << 'd'
DEBUG:root:{TICK: 133, ADDR: 10, PC: 0, OUT: 100, ACC: 100} 
DEBUG:root:{TICK: 134, ADDR: 10, PC: 1, OUT: 100, ACC: 100} 
DEBUG:root:{TICK: 135, ADDR: 14, PC: 1, OUT: 1, ACC: 1} 
DEBUG:root:{TICK: 136, ADDR: 13, PC: 1, OUT: 10, ACC: 11} 
DEBUG:root:{TICK: 137, ADDR: 13, PC: 1, OUT: 11, ACC: 11} 
DEBUG:root:{TICK: 138, ADDR: 13, PC: 2, OUT: 11, ACC: 11} 
DEBUG:root:{TICK: 139, ADDR: 13, PC: 2, OUT: 11, ACC: 11} 
DEBUG:root:{TICK: 140, ADDR: 15, PC: 2, OUT: 12, ACC: 1} 
DEBUG:root:{TICK: 141, ADDR: 15, PC: 3, OUT: 12, ACC: 1} 
DEBUG:root:{TICK: 142, ADDR: 15, PC: 4, OUT: 12, ACC: 1} 
DEBUG:root:{TICK: 143, ADDR: 15, PC: 0, OUT: 12, ACC: 1} 
DEBUG:root:{TICK: 144, ADDR: 13, PC: 0, OUT: 11, ACC: 11} 
DEBUG:root:{TICK: 145, ADDR: 11, PC: 0, OUT: 33, ACC: 33} 
DEBUG:root:output: 'Hello world' << '!'
DEBUG:root:{TICK: 146, ADDR: 11, PC: 0, OUT: 33, ACC: 33} 
DEBUG:root:{TICK: 147, ADDR: 11, PC: 1, OUT: 33, ACC: 33} 
DEBUG:root:{TICK: 148, ADDR: 14, PC: 1, OUT: 1, ACC: 1} 
DEBUG:root:{TICK: 149, ADDR: 13, PC: 1, OUT: 11, ACC: 12} 
DEBUG:root:{TICK: 150, ADDR: 13, PC: 1, OUT: 12, ACC: 12} 
DEBUG:root:{TICK: 151, ADDR: 13, PC: 2, OUT: 12, ACC: 12} 
DEBUG:root:{TICK: 152, ADDR: 13, PC: 2, OUT: 12, ACC: 12} 
DEBUG:root:{TICK: 153, ADDR: 15, PC: 2, OUT: 12, ACC: 0} 
DEBUG:root:{TICK: 154, ADDR: 15, PC: 3, OUT: 12, ACC: 0} 
DEBUG:root:{TICK: 155, ADDR: 15, PC: 5, OUT: 12, ACC: 0} 
INFO:root:output_buffer: 'Hello world!'
Hello world!
instr_counter: 59 ticks: 155

Process finished with exit code 0

```

| ФИО             | алг.  | code байт | code инстр. | инстр. | такт.  | вариант                                                |
|-----------------|-------|-----------|-------------|--------|--------|--------------------------------------------------------|
| Хафизов Б.Л.    | hello | -         | 6           | 59     | 214    | asm, cisc, harv, mc, tick, struct, stream, mem, prob2 |
| Хафизов Б.Л.    | cat   | -         | 4           | 21     | 64     | asm, cisc, harv, mc, tick, struct, stream, mem, prob2 |
| Хафизов Б.Л.    | prob2 | -         | 14          | 256    | 883    | asm, cisc, harv, mc, tick, struct, stream, mem, prob2 |
