.data:
    hw str "Hello world!"
    cur_ind num 0
.text:
loop:
    out# cur_ind
    add 1 cur_ind
    cmp cur_ind 12
    je end
    jmp loop
end:
    hlt