# !/usr/bin/python3
# pylint: disable=too-many-instance-attributes
# pylint: disable=invalid-name
# pylint: disable=consider-using-f-string
# pylint: disable=too-many-branches
# pylint: disable=too-many-statements#
# pylint: disable=no-self-use

import logging
import sys
from isa import Opcode, read_code


class DataPath:

    def __init__(self, data_memory_size, input_buffer, data_section):
        assert data_memory_size > 0, "Data_memory size should be non-zero!"
        self.data_memory_size = data_memory_size
        self.data_memory = [0] * data_memory_size
        self.data_address, self.alu, self.acc, self.br = 0, 0, 0, 0
        self.input_buffer = input_buffer
        self.output_buffer = []
        for var_addr, variable in enumerate(data_section):
            self.data_memory[var_addr] = int(variable)

    def latch_data_addr(self, addr):
        assert 0 <= int(addr) <= self.data_memory_size, "Out of memory!"
        self.data_address = int(addr)

    def latch_acc(self, sel):
        if sel == "MEM":
            self.acc = self.data_memory[self.data_address]
        elif sel == "ALU":
            self.acc = self.alu
        elif sel == "INPUT":
            if len(self.input_buffer) == 0:
                raise EOFError()
            symbol = self.input_buffer.pop(0)
            assert 0 <= ord(symbol) <= 127, \
                "input character is out of bound: {}".format(ord(symbol))
            self.acc = ord(symbol)

    def latch_br(self):
        self.br = self.alu

    def alu_sel_op(self, operation, sel_left, sel_right):
        if sel_left == "MEM":
            left_value = self.data_memory[self.data_address]
        else:
            left_value = self.br
        right_value = self.acc if sel_right == "ACC" else 0
        self.alu = left_value
        if operation == Opcode.TEST:
            self.alu &= right_value
        if operation == Opcode.CMP:
            self.alu -= right_value
        if operation == Opcode.ADD:
            self.alu += right_value

    def output(self, is_char):
        symbol = chr(self.acc) if is_char else str(self.acc)
        logging.debug('output: %s << %s', repr(
            ''.join(self.output_buffer)), repr(symbol))
        self.output_buffer.append(symbol)

    def write(self, value):
        self.data_memory[self.data_address] = value

    def zero(self):
        return self.acc == 0


class ControlUnit:

    def __init__(self, data_path, program):
        self.data_path = data_path
        self.program = program
        self._tick = 0
        self.program_counter = 0
        self.args = []
        self.mc_memory = [self.latch_microcode, self.read_first_arg, self.alu_add, self.alu_test, self.alu_cmp,
                          self.out_num, self.read_from_acc_addr, self.out_char, self.write_acc_by_second_arg,
                          self.write_from_input, self.next_instr]
        self.mc_counter = 0

    def tick(self):
        self._tick += 1
        logging.debug('%s', self)

    def current_tick(self):
        return self._tick

    def current_args(self):
        return self.args

    def decode_and_execute_instruction(self):
        self.mc_counter = 0
        instr = self.program[self.program_counter]
        self.args = instr["args"]
        while self.mc_counter < 11:
            self.mc_memory[self.mc_counter]()
            self.tick()

    def latch_microcode(self):
        opcode = self.program[self.program_counter]["opcode"]
        if opcode == Opcode.IN:
            self.mc_counter += 9
        elif opcode in [Opcode.JMP, Opcode.JE]:
            self.mc_counter += 10
        elif opcode == Opcode.HLT:
            raise StopIteration()
        else:
            self.mc_counter += 1

    def read_first_arg(self):
        opcode = self.program[self.program_counter]["opcode"]
        self.data_path.latch_data_addr(self.args[0])
        self.data_path.latch_acc("MEM")
        next_mc = {Opcode.ADD: 1, Opcode.TEST: 2, Opcode.CMP: 3, Opcode.OUT: 4, Opcode.OUT_REL: 5,
                   Opcode.OUT_CHAR: 6, Opcode.MOV: 7}
        self.mc_counter += next_mc[opcode]

    def write_from_input(self):
        self.data_path.latch_acc("INPUT")
        self.data_path.latch_data_addr(self.args[0])
        self.data_path.write(self.data_path.acc)
        self.mc_counter += 1

    def read_from_acc_addr(self):
        self.data_path.latch_data_addr(self.data_path.acc)
        self.data_path.latch_acc("MEM")
        self.mc_counter += 1

    def write_acc_by_second_arg(self):
        self.data_path.latch_data_addr(self.args[1])
        self.data_path.write(self.data_path.acc)
        self.mc_counter += 2

    def alu_cmp(self):
        self.data_path.latch_data_addr(self.args[1])
        self.data_path.alu_sel_op(Opcode.CMP, "MEM", "ACC")
        self.data_path.latch_acc("ALU")
        self.mc_counter += 6

    def alu_test(self):
        self.data_path.latch_data_addr(self.args[1])
        self.data_path.alu_sel_op(Opcode.TEST, "MEM", "ACC")
        self.data_path.latch_acc("ALU")
        self.mc_counter += 7

    def alu_add(self):
        self.data_path.latch_data_addr(self.args[1])
        self.data_path.alu_sel_op(Opcode.ADD, "MEM", "ACC")
        self.data_path.latch_acc("ALU")
        self.mc_counter += 6

    def out_num(self):
        self.data_path.output(is_char=False)
        self.mc_counter += 5

    def out_char(self):
        self.data_path.output(is_char=True)
        self.mc_counter += 3

    def next_instr(self):
        opcode = self.program[self.program_counter]["opcode"]
        self.args = self.program[self.program_counter]["args"]
        if opcode == Opcode.JMP:
            self.program_counter = self.args[0]
        elif opcode == Opcode.JE:
            self.program_counter = self.args[0] if self.data_path.zero() else self.program_counter + 1
        else:
            self.program_counter += 1
        self.mc_counter += 1

    def stop_program(self):
        raise StopIteration

    def __repr__(self):
        state = "{{TICK: {}, ADDR: {}, PC: {}, OUT: {}, ACC: {}}}".format(
            self._tick,
            self.data_path.data_address,
            self.program_counter,
            self.data_path.data_memory[self.data_path.data_address],
            self.data_path.acc,
        )

        return "{} ".format(state)


def simulation(code, input_tokens, data_memory_size, limit, data_section):
    data_path = DataPath(data_memory_size, input_tokens, data_section)
    control_unit = ControlUnit(data_path, code)
    instr_counter = 0
    logging.debug('%s', control_unit)
    try:
        while True:
            assert limit > instr_counter, "too long execution, increase limit!"
            control_unit.decode_and_execute_instruction()
            instr_counter += 1
    except EOFError:
        logging.warning('Input buffer is empty!')
    except StopIteration:
        pass
    logging.info('output_buffer: %s', repr(''.join(data_path.output_buffer)))
    return ''.join(data_path.output_buffer), instr_counter, control_unit.current_tick()


def main(args):
    assert 1 <= len(args) <= 2, "Wrong arguments: machine.py <code_file> [ <input_file> ]"
    code_file, input_file = args[0], args[1] if len(args) == 2 else ''
    code, data_section = read_code(code_file)
    if input_file != '':
        with open(input_file, encoding="utf-8") as file:
            input_text = file.read()
            input_token = []
            for char in input_text:
                input_token.append(char)
    else:
        input_token = []

    output, instr_counter, ticks = simulation(code,
                                              input_tokens=input_token,
                                              data_memory_size=100, limit=1000000000,
                                              data_section=data_section)

    print(output)
    print("instr_counter:", instr_counter, "ticks:", ticks)
    return ''.join(output)


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.DEBUG)
    main(sys.argv[1:])
