# !/usr/bin/python3
# pylint: disable=too-many-locals
# pylint: disable=invalid-name
# pylint: disable=consider-using-f-string
# pylint: disable=too-many-branches
# pylint: disable=too-many-statements
# pylint: disable=too-many-nested-blocks

import sys

from isa import Opcode, write_code

command2opcode = {
    'add': Opcode.ADD,
    'mov': Opcode.MOV,
    'test': Opcode.TEST,
    'in': Opcode.IN,
    'out': Opcode.OUT,
    'out_char': Opcode.OUT_CHAR,
    'out#': Opcode.OUT_REL,
    'jmp': Opcode.JMP,
    'cmp': Opcode.CMP,
    'je': Opcode.JE,
    'hlt': Opcode.HLT,
}


def translate(text):
    code = []
    labels = {}
    was_text_section = False
    addr_cnt = 0
    data = []
    for line in text.split("\n"):
        if line.strip() == ".text:":
            was_text_section = True
            continue
        if was_text_section:
            line_words = line.split()
            if len(line_words) == 1 and line_words[0][0] != "." and line_words[0][-1] == ":":
                assert line_words[0][0:-1] not in labels, "Should be one label definition!"
                labels[line_words[0][0:-1]] = addr_cnt
            else:
                addr_cnt += 1
    assert was_text_section is True, "Section .text is not presented!"
    addr_cnt = 0
    variables = {}
    current_section = ""
    for line in text.split("\n"):
        if line in [".data:", ".text:"]:
            current_section = line
            continue
        if current_section == ".data:":
            if line != ".text:":
                variable = line.split(maxsplit=2)
                assert variable[0] not in variables, "Should be one variable definition!"
                if variable[1] == "str":
                    variables[variable[0]] = len(data)
                    for char in variable[2][1:-1]:
                        data.append(ord(char))
                    data.append(ord('\u0000'))
                else:
                    variables[variable[0]] = len(data)
                    data.append(variable[2])
        else:
            args = []
            command = line.split()
            if command[0][-1] == ":":
                continue
            for i in range(1, len(command)):
                if command[i][0] == '[' and command[i][-1] == ']':
                    if command[i][1:-1] in variables:
                        data.append(variables[command[i][1:-1]])
                        args.append(len(data) - 1)
                else:
                    if command[i] in variables:
                        args.append(variables[command[i]])
                    elif command[i] in labels:
                        args.append(labels[command[i]])
                    else:
                        data.append(command[i])
                        args.append(len(data) - 1)
            code.append({"opcode": command2opcode[command[0]],
                         "addr": addr_cnt,
                         "args": args})
            addr_cnt += 1
    return code, data


def main(args):
    assert len(args) == 2, "Wrong arguments: translator.py <input_file> <target_file>"
    source, target = args
    with open(source, "rt", encoding="utf-8") as f:
        source = f.read()
    code, data = translate(source)
    print("source LoC:", len(source.split()), "code instr:", len(code))
    write_code(target, code, data)


if __name__ == '__main__':
    main(sys.argv[1:])
