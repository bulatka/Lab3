.data:
    a num 1
    b num 2
    c num 0
    ans num 2
    max num 5702887
.text:
loop:
    mov b c
    add a b
    mov c a
    cmp b max
    je exit
    test b 1
    je even
    jmp loop
even:
    add b ans
    jmp loop
exit:
    out ans
    hlt