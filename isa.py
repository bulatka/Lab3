# !/usr/bin/python3

import json
from enum import Enum


class Opcode(str, Enum):
    ADD = 'add'
    MOV = 'mov'
    TEST = 'test'
    IN = 'in'
    OUT = 'out'
    OUT_CHAR = 'out_char'
    OUT_REL = 'out_rel'
    JMP = 'jmp'
    CMP = 'cmp'
    JE = 'je'
    HLT = 'hlt'


def write_code(filename, code, data):
    with open(filename, "w", encoding="utf-8") as file:
        file.write(json.dumps(code, indent=4))
    with open("data_file", "w", encoding="utf-8") as file:
        file.write(json.dumps(data, indent=4))


def read_code(filename):
    with open(filename, encoding="utf-8") as file:
        code = json.loads(file.read())
    with open("data_file", encoding="utf-8") as file:
        data_section = json.loads(file.read())
    return code, data_section
